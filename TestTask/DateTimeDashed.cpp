#include "datetimedashed.h"

DateTimeDashed::DateTimeDashed()
{
    _currentDateTimeFormat = "hh/mm/ss dd/MM/yyyy";
}

QString DateTimeDashed::makeDataTimeString(QString format)
{
    QDateTime currentQDateTime = QDateTime::currentDateTime ();
    QString formattedQDateTimeString = currentQDateTime.toString(_currentDateTimeFormat);
    return(formattedQDateTimeString);
}
void DateTimeDashed::getDataTimeString(QString &dateTimeString)
{

    dateTimeString = makeDataTimeString(_currentDateTimeFormat);

}
