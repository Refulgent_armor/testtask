#ifndef DATETIMEDASHED_H
#define DATETIMEDASHED_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include "basedatetime.h"

class DateTimeDashed:public BaseDateTime
{
    Q_OBJECT
private:
    QString _currentDateTimeFormat;
    QString makeDataTimeString(QString format);
public:
    DateTimeDashed();
public slots:
    void getDataTimeString(QString &dateTimeString);


};



#endif // DATETIMEDASHED_H
