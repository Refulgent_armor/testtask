#ifndef ACTIONSLOGGER_H
#define ACTIONSLOGGER_H

#include <QObject>
#include <QString>
#include <QFile>

class ActionsLogger : public QObject
{
    Q_OBJECT
public:


    static ActionsLogger& getInstance()
    {
        _filename = "logfile.txt";
        static ActionsLogger instance;
        return instance;
    }
    static bool writeChanges(QString text);
signals:

public slots:
private:
    ActionsLogger(){};

    ActionsLogger(const ActionsLogger&);
    ActionsLogger& operator=(const ActionsLogger);

    static bool checkFileExists(QString filename);
    static bool makeLogFile(QString filename);
    static QString _filename;
};


#endif // ACTIONSLOGGER_H

