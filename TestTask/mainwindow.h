#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QLabel>
#include <QTimer>
#include "datetimedashed.h"
#include "datetimedotted.h"
#include "actionslogger.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    onMakeDottedClick(void);
    onMakeSlashedClick(void);
    MakeSlashedDateTimeString(QString &dateTimeString);
    MakeDottedDateTimeString(QString &dateTimeString);
public slots:
    void onTimerTick(void);


private slots:
    void on_MakeDotted_clicked();
    void on_MakeSlashed_clicked();

private:
    DateTimeDashed *_cDateTimeDashed;
    DateTimeDotted *_cDateTimeDotted;
    QLabel _dataTime;
    QTimer* _timer;
    QString _currentDateTimeFormat;
    Ui::MainWindow *ui;
    ActionsLogger & _cActionLogger = ActionsLogger::getInstance();

};


#endif // MAINWINDOW_H
