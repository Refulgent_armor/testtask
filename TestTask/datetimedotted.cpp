#include "datetimedotted.h"

DateTimeDotted::DateTimeDotted()
{
     _currentDateTimeFormat = "hh.mm.ss dd.MM.yyyy";

}

QString DateTimeDotted::makeDataTimeString(QString format)
{
    QDateTime currentQDateTime = QDateTime::currentDateTime ();
    QString formattedQDateTimeString = currentQDateTime.toString(_currentDateTimeFormat);
    return(formattedQDateTimeString);
}
void DateTimeDotted::getDataTimeString(QString &dateTimeString)
{

    dateTimeString = makeDataTimeString(_currentDateTimeFormat);


}
