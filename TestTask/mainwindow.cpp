#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include "datetimedashed.h"
#include "datetimedotted.h"
#include "actionslogger.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _cDateTimeDotted = new DateTimeDotted;
    _cDateTimeDashed = new DateTimeDashed;

    connect(this,SIGNAL(MakeDottedDateTimeString(QString&)),_cDateTimeDotted,SLOT(getDataTimeString(QString&)));
    connect(this,SIGNAL(MakeSlashedDateTimeString(QString&)),_cDateTimeDashed,SLOT(getDataTimeString(QString&)));
    _currentDateTimeFormat = "hh.mm.ss dd.MM.yyyy";
    _timer = new QTimer(this);
    _timer->setInterval(1000);

    connect(_timer,SIGNAL(timeout()),this,SLOT(onTimerTick()));
    _timer->start();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _timer;
}

void MainWindow::onTimerTick(void)
{
    QString DataTimeString;
    if (_currentDateTimeFormat == "hh.mm.ss dd.MM.yyyy")        
        emit MakeDottedDateTimeString(DataTimeString);
    else if (_currentDateTimeFormat == "hh/mm/ss dd/MM/yyyy")
         emit MakeSlashedDateTimeString(DataTimeString);
    ui->_dataTime->setText(DataTimeString);
}

void MainWindow::on_MakeDotted_clicked()
{

    if (_currentDateTimeFormat != "hh.mm.ss dd.MM.yyyy")
    {
        _cActionLogger.writeChanges("set Dotted Date and Time");
        _currentDateTimeFormat = "hh.mm.ss dd.MM.yyyy";
        emit onTimerTick();
    }

}

void MainWindow::on_MakeSlashed_clicked()
{
    if (_currentDateTimeFormat != "hh/mm/ss dd/MM/yyyy")
    {
        _cActionLogger.writeChanges("set Slashed Date and Time");
        _currentDateTimeFormat = "hh/mm/ss dd/MM/yyyy";
        emit onTimerTick();
    }
}
