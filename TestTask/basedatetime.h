#ifndef BASEDATETIME_H
#define BASEDATETIME_H
#include <QObject>
#include <QString>
class BaseDateTime:public QObject
{
    Q_OBJECT
protected:
    QString makeDataTimeString(QString format);
public:
    QString getDataTimeString(QString dateTimeString);
};
#endif // BASEDATETIME_H
