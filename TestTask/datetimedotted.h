#ifndef DATETIMEDOTTED_H
#define DATETIMEDOTTED_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include "basedatetime.h"
class DateTimeDotted:public BaseDateTime
{
    Q_OBJECT
private:
    QString _currentDateTimeFormat;
    QString makeDataTimeString(QString format);
public:
    DateTimeDotted();

public slots:
    void getDataTimeString(QString &dateTimeString);


};



#endif // DATETIMEDOTTED_H
