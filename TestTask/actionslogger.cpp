#include "actionslogger.h"
#include "QTextStream"
#include "QDateTime"
#include <iomanip>
QString ActionsLogger::_filename;
bool ActionsLogger::checkFileExists(QString filename)
{
    QFile file;
    file.setFileName(filename);
    if (file.exists())
        return (true);
    else
        return(false);


}

bool ActionsLogger::writeChanges(QString text)
{
    if (checkFileExists(ActionsLogger::_filename))
    {
        QFile file;
        QDateTime dateTimeOfAction =  QDateTime::currentDateTime ();
        QString formattedQDateTimeString = dateTimeOfAction.toString("yyyy.MM.dd hh:mm:ss");
        file.setFileName(ActionsLogger::_filename);
        QTextStream out(&file);
        if(file.open(QIODevice::Append| QIODevice :: Text ))
        {
            out<<qSetFieldWidth(30)<<left<<formattedQDateTimeString<<qSetFieldWidth(0)<<text+'\n';;
            file.close();
            return(true);
        }
        else
            return (false);

    }
    else{
        if (makeLogFile(ActionsLogger::_filename))
            writeChanges(text.toUtf8());
        else return(false);

    }
    return (false);
}

bool ActionsLogger::makeLogFile(QString filename)
{
    QFile file;

    file.setFileName(filename);
    if(!file.open(QIODevice::WriteOnly | QIODevice :: Text ))
    {
       return (0);
    }
    QTextStream out(&file);
    out<<"Log file:"<<'\n';
    out<<qSetFieldWidth(30)<<left<<"time/date"<<qSetFieldWidth(0)<<"action\n";
    file.close();
    return(true);

}
